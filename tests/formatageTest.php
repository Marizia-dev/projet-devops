<?php

use PHPUnit\Framework\TestCase;

require_once './utile/formatage.php';

class StyleFunctionsTest extends TestCase
{
    public function testStyleTitreNiveau1()
    {
        $text = "Test Heading 1";
        $color = "red"; // Replace with your desired color
        $expectedResult = "<h1 class='text-center my-3 red perso_policeTitre perso_textShadow'>Test Heading 1</h1>";

        $result = styleTitreNiveau1($text, $color);

        $this->assertEquals($expectedResult, $result);
    }

    public function testStyleTitreNiveau2()
    {
        $text = "Test Heading 2";
        $color = "blue"; // Replace with your desired color
        $expectedResult = "<h2 class='text-center my-3 blue perso_policeTitre perso_textShadow'>Test Heading 2</h2>";

        $result = styleTitreNiveau2($text, $color);

        $this->assertEquals($expectedResult, $result);
    }

    public function testStyleTitreNiveau3()
    {
        $text = "Test Heading 3";
        $color = "green"; // Replace with your desired color
        $expectedResult = "<h2 class='text-center my-3 green perso_policeTitre perso_textShadow perso_size26'>Test Heading 3</h2>";

        $result = styleTitreNiveau3($text, $color);

        $this->assertEquals($expectedResult, $result);
    }

    public function testStyleTitrePost()
    {
        $text = "Test Post Heading";
        $expectedResult = "<h2 class='text-center my-3 perso_policeTitre perso_textShadow border-bottom border-dark'>Test Post Heading</h2>";

        $result = styleTitrePost($text);

        $this->assertEquals($expectedResult, $result);
    }
}
