FROM php:7.4-apache

# Copy the application files into the container
COPY . /var/www/html/

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# Install zip, unzip, and git
RUN apt-get update \
    && apt-get install -y \
        zip \
        unzip \
        git \
    && rm -rf /var/lib/apt/lists/*

# Install PHPUnit
RUN curl --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit-6.5.phar" \
    && chmod +x /usr/local/bin/phpunit

# Install PHP extensions and enable Apache rewrite module
RUN docker-php-ext-install mysqli \
    && a2enmod rewrite
